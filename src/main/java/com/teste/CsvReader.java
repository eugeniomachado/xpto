package com.teste;

import com.teste.city.CityDTO;
import com.teste.city.CityService;

import javax.inject.Inject;
import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CsvReader {

    @Inject
    private CityService service;

    public void readFile() {

        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {

            br = new BufferedReader(new InputStreamReader((
                    this.getClass().getClassLoader().getResourceAsStream("Trabalho Java - Cidades.csv"))));
            while ((line = br.readLine()) != null) {
                try {
                    String[] city = line.split(cvsSplitBy);
                    CityDTO cityDTO = new CityDTO();

                    cityDTO.setIbge_id(Long.parseLong(city[0]));
                    cityDTO.setUf(city[1]);
                    cityDTO.setName(city[2]);
                    cityDTO.setCapital(Boolean.parseBoolean(city[3]));
                    cityDTO.setLat(Float.parseFloat(city[4]));
                    cityDTO.setLon(Float.parseFloat(city[5]));
                    cityDTO.setNo_accents(city[6]);
                    cityDTO.setAlternative_names(city[7]);
                    cityDTO.setMicroregion(city[8]);
                    cityDTO.setMesoregion(city[9]);

                    persist(cityDTO);

                }catch (NumberFormatException e){
                    System.out.println("not a number");
                }

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void persist(CityDTO cityDTO) {
        service.save(cityDTO);

    }

    public List<String> readColumn(String column, String filter) {

        BufferedReader br = null;
        String line;
        String cvsSplitBy = ",";
        List<String> res = new ArrayList<>();

        boolean isFirst = true;
        int pos = 0;
        try {
            br = new BufferedReader( new InputStreamReader((
                    this.getClass().getClassLoader().getResourceAsStream("Trabalho Java - Cidades.csv"))));
            while ((line = br.readLine()) != null) {
                try {
                    String[] col = line.split(cvsSplitBy);
                    if (isFirst) {
                        int index = 0;
                        for (String s : col) {
                            if (s.equals(column)) {
                                pos = index;
                                break;
                            }
                            index++;
                        }
                        isFirst = false;
                    } else {

                        if(col[pos].equalsIgnoreCase(filter)) {
                            res.add(col[pos]);
                        }
                    }
                } catch (NumberFormatException e) {
                    System.out.println("not a number");
                }

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return res;
    }

}

