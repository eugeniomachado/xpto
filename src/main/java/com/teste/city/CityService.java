package com.teste.city;

import com.teste.CsvReader;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class CityService {
    @Inject
    private CityDAO dao;
    @Inject
    private CsvReader csvReader;

    @Transactional
    public City save(CityDTO dto) {
        City city = new City();
        city.setIbge_id(dto.getIbge_id());
        city.setName(dto.getName());
        city.setUf(dto.getUf());
        city.setCapital(dto.getCapital());
        city.setLon(dto.getLon());
        city.setLat(dto.getLat());
        city.setNo_accents(dto.getNo_accents());
        city.setAlternative_names(dto.getAlternative_names());
        city.setMicroregion(dto.getMicroregion());
        city.setMesoregion(dto.getMesoregion());

        return dao.save(city);
    }

    public City findById(Long id) {
        return dao.findById(id);
    }

    public List<City> find() {
        return dao.find();
    }

    @Transactional
    public City update(CityDTO dto) {
        City city = dao.findById(dto.getId());
        return city;
    }

    @Transactional
    public City delete(Long id) {
        City city = dao.findById(id);
        dao.delete(city);
        return city;
    }

    public void importCsv() {
        csvReader.readFile();
    }

    public List<City> getCityByCapitalAndOrder() {
        return dao.getCityByCapitalAndOrder();
    }

    public StateVO getCitiesNumberState() {

       Object[] max = (Object[]) dao.getMaxStateCitiesCount();
       Object[] min = (Object[]) dao.getMinStateCitiesCount();

       StateVO stateVo = new StateVO((String) max[0], (BigInteger) max[1], (String) min[0], (BigInteger) min[1]);
       return stateVo;
    }

    public List<City> countCitiesByState() {
        return dao.countCitiesByState();
    }

    public City getByIbgeId(Long id) {
        return dao.getByIbgeId(id);
    }

    public List<String> getCityByState(String uf) {
        return dao.getCityByState(uf);
    }

    public Long findByColumn(String column) {
        return dao.findByColumn(column);
    }

    public Long countAll() {
        return dao.countAll();
    }

    public List<String> readCsvColumn(String column, String filter) {
        return csvReader.readColumn(column, filter);
    }

    public List<Object> moreDistantCities() {
        List<Object> list = new ArrayList<>();
        list.add(dao.getFarToTheNorthCity());
        list.add(dao.getFarToSouthorthCity());
        return list;
    }

}
