package com.teste.city;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path("/city")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CityResource {
    @Inject
    private CityService service;

    @POST
    public Response create(@NotNull CityDTO dto) {
        return Response.status(Response.Status.CREATED)
                .entity(new CityDTO(service.save(dto)))
                .build();
    }

    @GET
    public List<CityDTO> find() {
        return service.find().stream()
                .map(CityDTO::new)
                .collect(Collectors.toList());
    }

    @GET
    @Path("/{id}")
    public CityDTO findById(@PathParam("id") Long id) {
        return new CityDTO(service.findById(id));
    }

    @PUT
    public CityDTO update(@NotNull CityDTO dto) {
        return new CityDTO(service.update(dto));
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") Long id) {
        service.delete(id);
        return Response.noContent().build();
    }

    @GET
    @Path("/import")
    public void importCsv() {
        service.importCsv();
    }

    @GET
    @Path("/getCapital")
    public List<City> getCityOrder() {
        return service.getCityByCapitalAndOrder();
    }

    @GET
    @Path("/getStateCities")
    public Object getCitiesNumberState() {
        return service.getCitiesNumberState();
    }

    @GET
    @Path("/countCities")
    public List<City> countCitiesByState() {
        return service.countCitiesByState();
    }

    @GET
    @Path("/ibge/{id}")
    public City getByIbgeId(@PathParam("id") Long id) {
        return service.getByIbgeId(id);
    }

    @GET
    @Path("/getCitiesByState/{uf}")
    public List<String> getCitiesByState(@PathParam("uf") String uf) {
        return service.getCityByState(uf);
    }

    @DELETE
    @Path("/{id}")
    public Response deleteCity(@PathParam("id") Long id) {
       service.delete(id);
       return Response.status(Response.Status.NO_CONTENT).build();
    }

    @GET
    @Path("/countColumn/{column}")
    public Long findByColumn(@PathParam("column") String column) {
        return service.findByColumn(column);
    }

    @GET
    @Path("/countAll")
    public Long countAll() {
        return service.countAll();
    }

    @GET
    @Path("/readColumn/{column}/{filter}")
    public List<String> readColumn(@PathParam("column") String column,
                                   @PathParam("filter") String filter) {
        return service.readCsvColumn(column, filter);
    }

    @GET
    @Path("/distantCities")
    public List<Object> farestCities() {
        return service.moreDistantCities();
    }
}