package com.teste.city;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class CityDAO {
    @PersistenceContext(unitName = "senior")
    protected EntityManager em;

    @Transactional
    public City save(City entity) {
        em.persist(entity);
        return entity;
    }

    public City findById(Long id) {
        return em.find(City.class, id);
    }

    public List<City> find() {
        return em.createQuery("select ent from City ent", City.class)
                .getResultList();
    }

    public void delete(City entity) {
        em.remove(entity);
    }

    public List<City> getCityByCapitalAndOrder() {
        return em.createQuery("select ent from City ent where ent.capital = true  order by ent.name", City.class)
                .getResultList();
    }

    public Object getMaxStateCitiesCount() {
       return em.createNativeQuery("SELECT uf, count(*) FROM City GROUP BY uf order by count(*) desc limit 1;")
                .getSingleResult();

    }

    public Object getMinStateCitiesCount() {
        return em.createNativeQuery("SELECT uf, count(*) FROM City GROUP BY uf order by count(*) limit 1")
                .getSingleResult();
    }

    public List<City> countCitiesByState() {
        return em.createNativeQuery("SELECT uf, count(*) FROM City GROUP BY uf order by count(*)")
                .getResultList();
    }

    public City getByIbgeId(Long id) {
        return em.createQuery("select ent from City ent where ent.ibge_id = :id", City.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    public List<String> getCityByState(String uf) {
        return em.createQuery("select ent.name from City ent where ent.uf = :uf", String.class)
                .setParameter("uf", uf)
                .getResultList();
    }

    public Long findByColumn(String column) {
        return em.createQuery("select count(distinct ent."+column+") from City ent", Long.class)
                .getSingleResult();
    }

    public Long countAll() {
        return em.createQuery("select count(c) from City c", Long.class)
                .getSingleResult();
    }

    public Object getFarToTheNorthCity() {
        return em.createNativeQuery("select * from City ORDER BY City.lon desc limit 1").getSingleResult();
    }

    public Object getFarToSouthorthCity() {
        return em.createNativeQuery("SELECT * FROM City ORDER BY City.lon LIMIT 1").getSingleResult();
    }
}
