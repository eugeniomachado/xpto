package com.teste.city;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
@Entity
public class City {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long ibge_id;
    private String uf;
    private String name;
    private Boolean capital;
    private Float lon;
    private Float lat;
    private String no_accents;
    private String alternative_names;
    private String microregion;
    private String mesoregion;
}
