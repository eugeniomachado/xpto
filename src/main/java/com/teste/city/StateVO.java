package com.teste.city;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigInteger;


public class StateVO implements Serializable {

    private String ufMax;
    private String ufMin;
    private BigInteger countMax;
    private BigInteger countMin;

    public StateVO(String ufMax, BigInteger countMax, String ufMin, BigInteger countMin) {
        this.ufMax = ufMax;
        this.ufMin = ufMin;
        this.countMax = countMax;
        this.countMin = countMin;
    }

    public String getUfMax() {
        return ufMax;
    }

    public void setUfMax(String ufMax) {
        this.ufMax = ufMax;
    }

    public String getUfMin() {
        return ufMin;
    }

    public void setUfMin(String ufMin) {
        this.ufMin = ufMin;
    }

    public BigInteger getCountMax() {
        return countMax;
    }

    public void setCountMax(BigInteger countMax) {
        this.countMax = countMax;
    }

    public BigInteger getCountMin() {
        return countMin;
    }

    public void setCountMin(BigInteger countMin) {
        this.countMin = countMin;
    }
}
