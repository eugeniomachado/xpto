package com.teste.city;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CityDTO {
    private Long id;
    private Long ibge_id;
    private String uf;
    private String name;
    private Boolean capital;
    private Float lon;
    private Float lat;
    private String no_accents;
    private String alternative_names;
    private String microregion;
    private String mesoregion;

    public CityDTO(City city) {
        this.id = city.getId();
        this.ibge_id = city.getIbge_id();
        this.uf = city.getUf();
        this.name =  city.getName();
        this.capital = city.getCapital();
        this.lon = city.getLon();
        this.lat =  city.getLat();
        this.no_accents = city.getNo_accents();
        this.alternative_names = city.getAlternative_names();
        this.microregion =  city.getMicroregion();
        this.mesoregion = city.getMesoregion();
    }


}
