# Desafio backend senior 

## Intro

> Tecnologias utiilizadas: Java, Thorntail e Lombok

> Banco de dados: MySQL

## Build

Para rodar o projeto:

> Compilar: mvn clean install

Criar uma database (Mysql) chamado senior :)

para subir a aplicação:

> acessar o diretorio /target/ 

rodar o comando

> java -jar senior-api-core-thorntail.jar


